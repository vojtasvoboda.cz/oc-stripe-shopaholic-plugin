# Stripe Payment Gateway for Shopaholic plugin

### Installation guide

Add **omnipay/stripe** and **ignited/laravel-omnipay** packages to the composer.json of your project.

```$xslt
{
    "require": [
     ...
        "ignited/laravel-omnipay": "3.*",
        "omnipay/stripe": "^3.1"
    ],

```

Execute below at the root of your project.
```
composer update
```

### Setting up the payment method

In Settings > Catalog Configuration > Payment Methods create new payment method. Choose Stripe payment gateway and create.

Enter your Stripe public key in "API Key public" field and your secret key in "Client secret" fields.

## PaymentCardForm component

Collecting user bank card details to process the order.

You have 2 options: make payment request right after order is created (using MakeOrder component) or first create the order, 
then collect card details for payment request and finally do the payment request (using OrderPage component).

#### Payment using MakeOrder component

In this approach you need to integrate stripe payment form 
([Stripe.js quickstart](https://stripe.com/docs/stripe-js/elements/quickstart)) with PaymentCardForm component and MakeOrder
form and generate a stripe token from the 
user's input. Don't forget to insert your public Stripe API key into sample stripe js code.
Then you need to save this token to the order record into the payment_token field using 
'shopaholic.order.after_create' event.

Also if the Stripe is default gateway your application uses - you can set auto assigning Stripe payment method to every 
order created.


#### Payment using OrderPage component

This approach allows to first create the order and then to collect user's card details separately. 
Here you need to use PaymentCardForm independently also integrating with 
([Stripe.js bank card form](https://stripe.com/docs/stripe-js/elements/quickstart)).
PaymentCardForm onUpdateToken method will save generated token to the order (expecting stripeToken post parameter).

```$xslt
[PaymentCardForm]
mode = "ajax"
redirect_on = 1
redirect_page = "order_success"
slug = "{{ :slug }}"
slug_required = 1
==
{% if PaymentCardForm.get() %}
<form method="post" id="payment-form">
    <div class="form-row">
        <label for="card-element">
            Credit or debit card
        </label>
        <div id="card-element">
            <!-- A Stripe Element will be inserted here. -->
        </div>

        <!-- Used to display form errors. -->
        <div id="card-errors" role="alert"></div>
    </div>

    <button>Submit Payment</button>
</form>
{% endif %}

<script>
// js code from Stripe.js quickstart link replacing public Stripe API key with your own
var stripe = Stripe({{ PaymentCardForm.get().payment_method.getObject.getProperty('api_key_public') }});
</script>
```

Then using OrderPage component and it's method onPurchase you can trigger the payment request to Stripe
using saved token as payment source.


#### Payment Intent example (thanks to Vojta Svoboda)

New method PaymentCardForm.getPaymentIntentClientSecret() available from v1.0.4. 

```$xslt
title = "Order complete"
url = "/checkout/:slug"
layout = "eshop"
is_hidden = 0

[OrderPage]
slug = "{{ :slug }}"
slug_required = 1

[PaymentCardForm]
mode = "ajax"
redirect_on = 1
redirect_page = "order-complete"
slug = "{{ :slug }}"
slug_required = 1
==
{% set payment = PaymentCardForm.get().payment_method.getObject() %}

{% if payment.code == "stripe" %}
    <form method="post" id="payment-form">
        <label for="card-element">Credit or debit card</label>
        <div id="card-element"></div>
        <div id="card-errors" role="alert"></div>
        <button>Submit payment</button>
    </form>

    {% set paymentIntent = PaymentCardForm.getPaymentIntentClientSecret() %}
    <script src="https://js.stripe.com/v3/"></script>
    <script>
      var stripe = Stripe('{{ payment.getProperty("api_key_public") }}');
      var elements = stripe.elements({
        'clientSecret': '{{ paymentIntent }}',
      });
      var card = elements.create('payment');
      card.mount('#card-element');

      $('#payment-form').on('submit', function (e) {
        e.preventDefault();
        stripe.createToken(card).then(function (result) {
          if (result.token) {
            // save token to the order
            $.request('PaymentCardForm::onUpdateToken', {
              'data': {
                'stripeToken': result.token.id,
              }
            });

            // trigger payment request
            $.request('OrderPage::onPurchase');
          }
        });
      });
    </script>
{% endif %}
```

## License

© 2019, under [GNU GPL v3](https://opensource.org/licenses/GPL-3.0).

Developed by [Artem Rybachuk](https://github.com/vdomah).
