<?php namespace Vdomah\StripeShopaholic;

use Cms\Classes\Page;
use Event;
use System\Classes\PluginBase;

use Vdomah\StripeShopaholic\Classes\Event\ExtendFieldHandler;
use Vdomah\StripeShopaholic\Classes\Event\ExtendOrderCreateHandler;

use Lovata\OmnipayShopaholic\Classes\Helper\PaymentGateway;
use Lovata\OrdersShopaholic\Models\PaymentMethod;
use Vdomah\StripeShopaholic\Classes\Helpers\OrderCheckoutDataHelper;

/**
 * Class Plugin
 * @package Vdomah\StripeShopaholic
 * @author Artem Rybachuk, alchemistt@ukr.net
 */
class Plugin extends PluginBase
{
    public $require = ['Lovata.Toolbox', 'Lovata.Shopaholic', 'Lovata.OrdersShopaholic', 'Lovata.OmnipayShopaholic'];

    /**
     * Register component plugin method
     * @return array PaymentMethod
     */
    public function registerComponents()
    {
        return [
            'Vdomah\StripeShopaholic\Components\PaymentCardForm' => 'PaymentCardForm',
        ];
    }

    /**
     * Boot plugin method
     */
    public function boot()
    {
        $this->addEventListener();

        PaymentMethod::extend(function ($obElement) {
            /** @var PaymentMethod $obElement */
            $obElement->addGatewayClass(
                ExtendFieldHandler::STRIPE_CHARGE_GATEWAY_ID,
                PaymentGateway::class
            );

            /** @var PaymentMethod $obElement */
            $obElement->addGatewayClass(
                ExtendFieldHandler::STRIPE_PAYMENT_INTENTS_GATEWAY_ID,
                PaymentGateway::class
            );

            /** @var PaymentMethod $obElement */
            $obElement->addGatewayClass(
                ExtendFieldHandler::STRIPE_CHECKOUT_GATEWAY_ID,
                PaymentGateway::class
            );
        });
    }

    /**
     * Add event listeners
     */
    protected function addEventListener()
    {
        Event::listen(PaymentMethod::EVENT_GET_GATEWAY_LIST, function () {
            return [
                ExtendFieldHandler::STRIPE_CHARGE_GATEWAY_ID => 'Stripe Charge',
                ExtendFieldHandler::STRIPE_PAYMENT_INTENTS_GATEWAY_ID => 'Stripe Payment Intents',
                ExtendFieldHandler::STRIPE_CHECKOUT_GATEWAY_ID => 'Stripe Checkout',
            ];
        });

        Event::listen(PaymentGateway::EVENT_GET_PAYMENT_GATEWAY_PURCHASE_DATA, function ($obOrder, $obPaymentMethod, $arPurchaseData) {
            if ($obPaymentMethod->gateway_id === ExtendFieldHandler::STRIPE_CHARGE_GATEWAY_ID) {
                return [
                    'apiKey' => $obPaymentMethod->getProperty('apiKey'),
                ];
            }

            if ($obPaymentMethod->gateway_id === ExtendFieldHandler::STRIPE_CHECKOUT_GATEWAY_ID) {
                // don't redirect to the Lovata.OmnipayShopaholic routes, because they are setting order to the
                // paid/canceled state without any check
                // override also returnUrl which is not needed for Stripe Checkout, but it contains danger URL
                $url = Page::url('order-complete', ['slug' => $obOrder->secret_key]);

                return [
                    'apiKey' => $obPaymentMethod->getProperty('apiKey'),
                    'returnUrl' => $url,
                    'successUrl' => $url . '?success=1',
                    'cancelUrl' => $url . '?cancel=1',
                    'lineItems' => OrderCheckoutDataHelper::getLineItems($obOrder),
                    'mode' => 'payment',
                    'customerEmail' => $obOrder->property['email'] ?? null,
                    'shippingOptions' => OrderCheckoutDataHelper::getShippingOptions($obOrder),
                ];
            }
        });

        Event::subscribe(ExtendFieldHandler::class);
        Event::subscribe(ExtendOrderCreateHandler::class);
    }
}
