<?php namespace Vdomah\StripeShopaholic\Components;

use Input;
use Event;
use Lovata\OrdersShopaholic\Classes\Helper\AbstractPaymentGateway;
use Lovata\OrdersShopaholic\Classes\Item\OrderItem;
use Omnipay\Omnipay;
use Redirect;

use Lovata\Toolbox\Classes\Helper\PageHelper;
use Lovata\Toolbox\Classes\Helper\UserHelper;
use Lovata\Toolbox\Classes\Component\ComponentSubmitForm;
use Lovata\Toolbox\Traits\Helpers\TraitValidationHelper;
use Lovata\Toolbox\Traits\Helpers\TraitComponentNotFoundResponse;

use Lovata\Shopaholic\Models\Settings;
use Lovata\OrdersShopaholic\Models\Order;
use Vdomah\StripeShopaholic\Classes\Event\ExtendOrderCreateHandler;

/**
 * Class PaymentCardForm
 * @package Vdomah\StripeShopaholic\Classes\Event
 * @author Artem Rybachuk, alchemistt@ukr.net
 */
class PaymentCardForm extends ComponentSubmitForm
{
    use TraitValidationHelper;
    use TraitComponentNotFoundResponse;

    /** @var \Lovata\OrdersShopaholic\Models\Order */
    protected $obOrder;

    /** @var  \Lovata\OrdersShopaholic\Classes\Item\OrderItem */
    protected $obOrderItem = null;

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'vdomah.stripeshopaholic::lang.component.payment_card_form_name',
            'description' => 'vdomah.stripeshopaholic::lang.component.payment_card_form_description',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        $arResult = $this->getModeProperty();

        $arResult = array_merge($arResult, $this->getElementPageProperties());

        return $arResult;
    }

    /**
     * Get element item
     * @return \Lovata\Toolbox\Classes\Item\ElementItem
     */
    public function get()
    {
        return $this->obOrderItem;
    }

    /**
     * Get redirect page property list
     * @return array
     */
    protected function getRedirectPageProperties()
    {
        if (!Result::status() || empty($this->obOrder)) {
            return [];
        }

        $arResult = [
            'id'     => $this->obOrder->id,
            'number' => $this->obOrder->order_number,
            'key'    => $this->obOrder->secret_key,
        ];

        $sRedirectPage = $this->property(self::PROPERTY_REDIRECT_PAGE);
        if (empty($sRedirectPage)) {
            return $arResult;
        }

        $arPropertyList = PageHelper::instance()->getUrlParamList($sRedirectPage, 'OrderPage');
        if (!empty($arPropertyList)) {
            $arResult[array_shift($arPropertyList)] = $this->obOrder->secret_key;
        }

        return $arResult;
    }

    /**
     * Init plugin method
     */
    public function init()
    {
        $this->bCreateNewUser = Settings::getValue('create_new_user');
        $this->obUser = UserHelper::instance()->getUser();

        parent::init();

        //Get element slug
        $sElementSlug = $this->property('slug');
        if (empty($sElementSlug)) {
            return;
        }

        //Get element by slug
        $this->obOrder = $this->getElementObject($sElementSlug);
        if (empty($this->obOrder)) {
            return;
        }

        $this->obOrderItem = $this->makeItem($this->obOrder->id, $this->obOrder);
    }

    /**
     * Set payment token
     * @return \Illuminate\Http\RedirectResponse|null
     * @throws \Exception
     */
    public function onRun()
    {
        $onSuccess = Input::get('success');
        if (!empty($onSuccess)) {
            $this->checkOrderPayment();
        }

        if ($this->sMode != self::MODE_SUBMIT) {
            return null;
        }

        $sPaymentToken = Input::get(ExtendOrderCreateHandler::STRIPE_TOKEN_PARAM_NAME);
        if (empty($sPaymentToken)) {
            return null;
        }

        $this->obOrder->payment_token = $sPaymentToken;
        $this->obOrder->save();

        $sRedirectURL = $this->property(self::PROPERTY_REDIRECT_PAGE);

        return $this->getResponseModeForm($sRedirectURL);
    }

    /**
     * Set payment token (AJAX)
     * @return \Illuminate\Http\RedirectResponse|array
     * @throws \Exception
     */
    public function onUpdateToken()
    {
        $sPaymentToken = Input::get(ExtendOrderCreateHandler::STRIPE_TOKEN_PARAM_NAME);

        $this->obOrder->payment_token = $sPaymentToken;
        $this->obOrder->save();

        $sRedirectURL = $this->property(self::PROPERTY_REDIRECT_PAGE);

        return $this->getResponseModeAjax($sRedirectURL);
    }

    /**
     * Get element object
     * @param string $sElementSlug
     * @return Order
     */
    protected function getElementObject($sElementSlug)
    {
        if (empty($sElementSlug)) {
            return null;
        }

        $this->obUser = UserHelper::instance()->getUser();

        $obElement = Order::getBySecretKey($sElementSlug)->first();
        if (!empty($obElement) && !empty($this->obUser) && $obElement->user_id != $this->obUser->id) {
            $obElement = null;
        }

        return $obElement;
    }

    /**
     * @param int    $iElementID
     * @param Order $obElement
     * @return OrderItem
     */
    protected function makeItem($iElementID, $obElement)
    {
        return OrderItem::make($iElementID, $obElement);
    }

    /**
     * @return string|null
     */
    public function getPaymentIntentClientSecret()
    {
        // payment method
        $paymentMethod = $this->obOrderItem->payment_method->getObject();

        // create gateway
        $gw = Omnipay::create($paymentMethod->gateway_id);
        $gw->initialize([
            'apiKey' => $paymentMethod->getProperty('apiKey'),
        ]);

        $paymentIntent = $gw->authorize([
            'amount' => $this->obOrder->total_price_data->price_with_tax_value,
            'currency' => $paymentMethod->gateway_currency,
            'payment_method' => 'card',
        ]);
        $data = $paymentIntent->getData();
        unset($data['payment_method']);

        $response = $paymentIntent->sendData($data);
        $data = $response->getData();

        return $data['client_secret'] ?? null;
    }

    /**
     * @return void
     */
    private function checkOrderPayment()
    {
        if (empty($this->obOrder->payment_response['response']['id'])) {
            return;
        }

        // init
        $paymentMethod = $this->obOrderItem->payment_method->getObject();
        $afterStatus = $paymentMethod->after_status;

        // skip if already paid
        if ($afterStatus !== null && ($this->obOrder->status_id === $afterStatus->id)) {
            return;
        }

        // get existing Stripe Checkout Session ID
        $session_id = $this->obOrder->payment_response['response']['id'];

        // create Stripe Checkout gateway
        $gw = Omnipay::create($paymentMethod->gateway_id);
        $gw->initialize([
            'apiKey' => $paymentMethod->getProperty('apiKey'),
        ]);

        // get Stripe Checkout Session
        $transaction = $gw->fetchTransaction();
        $transaction->setTransactionReference($session_id);
        $response = $transaction->send();
        $data = $response->getData();

        // set order as paid if status is complete
        if (isset($data['status']) && 'complete' === $data['status']) {
            if (!empty($afterStatus)) {
                $this->obOrder->status_id = $afterStatus->id;
                $this->obOrder->save();
                Event::fire(AbstractPaymentGateway::EVENT_PAYMENT_SUCCESS, [$this->obOrder]);

                // refresh the order
                $this->obOrderItem = $this->makeItem($this->obOrder->id, $this->obOrder);
            }
        }
    }
}
