<?php namespace Vdomah\StripeShopaholic\Classes\Helpers;

use Lovata\OrdersShopaholic\Models\Order;

class OrderCheckoutDataHelper
{
    public static function getLineItems(Order $order)
    {
        $items = [];

        foreach ($order->order_position as $item) {
            $items[] = [
                'price_data' => [
                    'currency' => strtolower($item->currency_code),
                    'product_data' => [
                        'name' => $item->offer->name,
                    ],
                    'unit_amount' => (int) ($item->price_with_tax_value * 100),
                ],
                'quantity' => $item->quantity,
            ];
        }

        return $items;
    }

    public static function getShippingOptions(Order $order)
    {
        $shipping_options = [];
        $shipping = [
            'shipping_rate_data' => [
                'type' => 'fixed_amount',
                'fixed_amount' => [
                    'amount' => $order->shipping_price_value,
                    'currency' => strtolower($order->currency_code),
                ],
                'display_name' => $order->shipping_type->name,
            ],
        ];
        $shipping_options[] = $shipping;

        return $shipping_options;
    }
}
